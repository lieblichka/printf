/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtreutel <dtreutel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/30 20:01:53 by dtreutel          #+#    #+#             */
/*   Updated: 2019/06/27 19:33:21 by dtreutel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"


static int	ft_handling_flags(t_printf *print)
{

}


static void	ft_parse_printf(t_printf *print)
{
	print->res = ft_strlenc(print->str, '%');
	write(1, print->str, print->res);
	print->str += print->res;
	while (*print->str)
	{
		if (*print->str == '%')
		{
			if (print->str[1] == '%')
			{
				ft_putchar('%');
				print->res += 1;
				print->str += 2;
			}
			else
				ft_handling_flags(print);

		}
	}
}

int			ft_printf(const char *format, ...)
{
	t_printf print;

	print.str = (char *)format;
	va_start(print.args, format);
	ft_parse_printf(&print);
	va_end(print.args);
	return (print.res);
}
