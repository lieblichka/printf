# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: dtreutel <dtreutel@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/04/05 14:34:56 by ramory-l          #+#    #+#              #
#    Updated: 2019/05/31 20:55:07 by dtreutel         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = libftprintf.a

PRF_DIR =./ft_printf

LFT_DIR =./libft

PRF_C = ft_printf.c

LFT_C =	ft_atoi.c ft_bzero.c ft_isalnum.c ft_isalpha.c \
		ft_isascii.c ft_isdigit.c ft_isprint.c ft_memccpy.c \
		ft_memchr.c ft_memcmp.c ft_memcpy.c ft_memmove.c \
		ft_memset.c ft_putchar.c ft_putnbr.c ft_putstr.c \
		ft_strcat.c ft_strchr.c ft_strcmp.c ft_strcpy.c \
		ft_strdup.c ft_strlcat.c ft_strlen.c ft_strncat.c \
		ft_strncmp.c ft_strncpy.c ft_strnstr.c ft_strrchr.c \
		ft_strstr.c ft_tolower.c ft_toupper.c ft_memalloc.c \
		ft_memdel.c ft_strnew.c ft_strdel.c ft_strclr.c \
		ft_striter.c ft_striteri.c ft_strmap.c ft_strmapi.c \
		ft_strequ.c ft_strnequ.c ft_strjoin.c ft_strtrim.c \
		ft_strsub.c ft_strlenc.c ft_strsplit.c ft_countwordc.c \
		ft_itoa.c ft_putendl.c ft_putchar_fd.c ft_putstr_fd.c \
		ft_putnbr_fd.c ft_putendl_fd.c ft_lstnew.c ft_lstdelone.c \
		ft_lstadd.c ft_lstdel.c ft_lstiter.c ft_lstmap.c \
		ft_lstdelcontent.c ft_arraydel.c ft_countchar.c \
		ft_swap_point.c ft_swap.c ft_power.c ft_sqrt.c

SRC_PRF = $(addprefix $(PRF_DIR)/, $(PRF_C))

SRC_LFT = $(addprefix $(LFT_DIR)/, $(LFT_C))

OBJ_PRF = $(SRC_PRF:.c=.o)

OBJ_LFT = $(SRC_LFT:.c=.o)

OBJ = $(OBJ_LFT) $(OBJ_PRF)

HEADERS = -I includes -I $(LFT_DIR)

IMFLAGS = -Wall -Wextra -Werror

LFT = -Llibft -lft

all: $(NAME)

$(NAME): $(OBJ_LFT) $(OBJ_PRF)
	ar rc $(NAME) $(OBJ_PRF) $(OBJ_LFT)

%.o:%.c
	gcc -c $< -o $@ $(IMFLAGS) $(HEADERS)

libft:
	make -C libft

clean:
	@rm -Rf $(OBJ)
	@make clean -C libft

fclean: clean
	@rm -Rf $(NAME)
	@make fclean -C libft

re: fclean all
