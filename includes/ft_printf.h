/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtreutel <dtreutel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/30 19:56:23 by dtreutel          #+#    #+#             */
/*   Updated: 2019/06/27 18:31:11 by dtreutel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H

#include "libft.h"
#include <stdlib.h>
#include <stdarg.h>
#include <unistd.h>

typedef struct	s_printf
{
	va_list		args;
	char		*str;
	int			res;
}				t_printf;

typedef struct	s_flags
{
	char		flag;
	char		width;
	char		precision;
	char		type;
}				t_flags;



int		ft_printf(const char *format, ...);

#endif
